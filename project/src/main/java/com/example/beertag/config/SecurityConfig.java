package com.example.beertag.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private DataSource dataSource;

    @Autowired
    public SecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(dataSource);
        return jdbcUserDetailsManager;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource);
    }


//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser(User.withUsername("amanis").password("{noop}amanis").roles("USER"))
//                .withUser(User.withUsername("admin").password("{noop}admin").roles("ADMIN"));
//    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                    .antMatchers("/images/**", "/css/**", "/js/**", "/fonts/**", "/vendor/**").permitAll()
                    .antMatchers("/", "/beers","/beers/{beerId}/info","/beers/sort","/beers/sort/**", "/register", "/register-confirmation").permitAll()
                    .antMatchers("/beers/new").hasRole("USER")
                    .antMatchers("/beers/remove/wishlist/**").hasRole("USER")
                    .antMatchers("/beers/{id}").hasRole("USER")
                    .antMatchers("/admin").hasRole("ADMIN")
                    .anyRequest()
                .authenticated()
                .and()
                    .formLogin()
                        .loginPage("/login")
                        .loginProcessingUrl("/authenticate")
                        .defaultSuccessUrl("/beers",true)
                        .permitAll()
                .and()
                    .logout()
                            .logoutSuccessUrl("/")
                             .permitAll()

                .and()
                .exceptionHandling()
                    .accessDeniedPage("/access_denied");
    }
}
