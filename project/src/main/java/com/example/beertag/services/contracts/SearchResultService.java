package com.example.beertag.services.contracts;

import com.example.beertag.models.Beer;

import java.util.List;

public interface SearchResultService {
    List<Beer> getList();

    List<Beer> addBeers(List<Beer> beersToAdd);

    String getLastSortedBy();

    void setLastSortedBy(String sort);
}
