package com.example.beertag.services.contracts;

import com.example.beertag.models.Beer;
import com.example.beertag.models.Tag;
import com.example.beertag.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface BeerService {

    List<Beer> getAllDeletedBeers();

    void create(Beer beer, User creator);

    Beer getById(int id);

    List<Beer> getAll();

    List<Beer> filterByName(String name);

    List<Beer> filterByStyle(int styleId);

    List<Beer> filterByCountry(int countryId);

    List<Beer> filterByCountry(String countryName);

    void update(Beer beer, User user);

    void delete(int id, User user);

    void addTagToBeer(String tagName, int beerId);

    void addListOfTagsToBeer(Set<Tag> tags, int beerId);

    List<Beer> sort(String filter, List<Beer> beers);

    List<Beer> sortByName ();

    List<Beer> sortByName(List<Beer> list);

    List<Beer> sortByAbv ();

    Set<Beer> sortByRating();

    List<Beer> searchByCriteria(String name, int styleId, int countryId, String tag);
}
