package com.example.beertag.services;

import com.example.beertag.exceptions.InvalidOperationException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.BeerRepository;
import com.example.beertag.repositories.contracts.RatingRepository;
import com.example.beertag.repositories.contracts.UserRepository;
import com.example.beertag.services.contracts.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RatingServiceImpl implements RatingService {

    private final RatingRepository ratingRepository;
    private final UserRepository userRepository;
    private final BeerRepository beerRepository;

    @Autowired
    public RatingServiceImpl(RatingRepository repository,
                             UserRepository userRepository,
                             BeerRepository beerRepository) {
        this.ratingRepository = repository;
        this.userRepository = userRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void rate(String username, int beerId, int rating) {
        User user = userRepository.getByUsername(username);
        Beer beer = beerRepository.getById(beerId);
        if (userRepository.getDrunkenBeers(user).stream().anyMatch(b -> b.getId() == beer.getId())) {
            ratingRepository.rate(user, beer, rating);
        } else {
            throw new InvalidOperationException(String.format("User %s doesn't have beer %s in his drunk list",
                    user.getUserName(),
                    beer.getName()));
        }
    }

    @Override
    public void unrateBeer(String username, int beerId) {
        User user = userRepository.getByUsername(username);
        Beer beer = beerRepository.getById(beerId);
        ratingRepository.unrateBeer(user, beer);
    }

    @Override
    public double calculateRating(int beerId) {
        Beer beer = beerRepository.getById(beerId);
        return ratingRepository.calculateRating(beer);
    }

    @Override
    public String printAvgRating(int beerId) {
        return ratingRepository.printAvgRating(beerRepository.getById(beerId));
    }
}
