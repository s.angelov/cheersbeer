package com.example.beertag.services.contracts;

import com.example.beertag.models.OriginCountry;

import java.util.List;

public interface OriginCountryService {

    List<OriginCountry> getAll();

    OriginCountry getById(int id);

    OriginCountry getByCountryName(String name);

    void create(OriginCountry originCountry);

    void update(OriginCountry originCountry);

    void delete(int id);
}
