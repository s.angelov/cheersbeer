package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.OriginCountry;
import com.example.beertag.repositories.contracts.OriginCountryRepository;
import com.example.beertag.services.contracts.OriginCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OriginCountryServiceImpl implements OriginCountryService {
    private final OriginCountryRepository repository;

    @Autowired
    public OriginCountryServiceImpl(OriginCountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<OriginCountry> getAll() {
        return repository.getAll();
    }

    @Override
    public OriginCountry getById(int id) {
        return repository.getById(id);
    }

    @Override
    public OriginCountry getByCountryName(String name) {
        return repository.getByCountryName(name);
    }

    @Override
    public void create(OriginCountry originCountry) {
        if (countryExist(originCountry.getCountry(), repository)) {
            throw new DuplicateEntityException(
                    String.format("Country with name %s already exist", originCountry.getCountry()));
        }
        repository.create(originCountry);
    }

    @Override
    public void update(OriginCountry originCountry) {
        if (!countryExist(originCountry.getId(), repository)) {
            throw new EntityNotFoundException(String.format("Country with name %s doesn't exist", originCountry.getCountry()));
        }
        repository.update(originCountry);
    }

    @Override
    public void delete(int id) {
        if (!countryExist(id, repository)) {
            throw new EntityNotFoundException(String.format
                    ("Country with name %s doesn't exist", repository.getById(id)));
        }
        repository.delete(id);

    }

    private boolean countryExist(String countryName, OriginCountryRepository repository) {
        return repository.getAll()
                .stream()
                .anyMatch(b -> b.getCountry().equalsIgnoreCase(countryName));
    }

    private boolean countryExist(int id, OriginCountryRepository repository) {
        return repository.getAll()
                .stream()
                .anyMatch(b -> b.getId() == id);
    }
}
