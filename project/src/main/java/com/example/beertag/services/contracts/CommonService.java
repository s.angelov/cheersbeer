package com.example.beertag.services.contracts;

import com.example.beertag.models.Style;

import java.util.List;

public interface CommonService<E> {

    List<E> getAll();

    E getById(int id);
}
