package com.example.beertag.services.contracts;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserService {

    void create(User user);

    User getById(int id);

    User getByUsername(String username);

    List<User> getAll();

    List<User> getAllDeletedUsers();

    void update(User user);

    void delete(int id);

    Set<Beer> getDrunkenBeers(int id);

    void addBeerToUserDrunkenList(int beerId, String username);

    Set<Beer> getWishList (int id);

    void addBeerToUserWishList(int beerId, String username);

    void removeFromList(String list, int userId, int beerId);

    //TODO
    //void addToWishList(int beerId);

}
