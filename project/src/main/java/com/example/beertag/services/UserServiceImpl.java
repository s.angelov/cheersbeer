package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.BeerRepository;
import com.example.beertag.repositories.contracts.UserRepository;
import com.example.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final BeerRepository beerRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BeerRepository beerRepository) {
        this.userRepository = userRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void create(User user) {
        if (!userRepository.filterByUsername(user.getUserName()).isEmpty()) {
            throw new DuplicateEntityException(String.format("User with username %s already exist.", user.getUserName()));
        }
        userRepository.add(user);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<User> getAllDeletedUsers() {
        return userRepository.getAllDeletedUsers();
    }

    @Override
    public void update(User user) {
        userRepository.getById(user.getId());
        userRepository.update(user);
    }

    @Override
    public void delete(int id) {
        userRepository.delete(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public Set<Beer> getDrunkenBeers(int id) {
        User user = userRepository.getById(id);
        Set<Beer> drunkenBeers = userRepository.getDrunkenBeers(user);
        return drunkenBeers;
    }

    @Override
    public void addBeerToUserDrunkenList(int beerId, String username) {
        Beer beer = beerRepository.getById(beerId);
        User user = userRepository.getByUsername(username);
        if (user.getDrunkenBeers().stream().anyMatch(b -> b.getId() == beerId)) {
            throw new DuplicateEntityException(String.format
                    ("Beer with name %s is already added to the drunkList", beer.getName()));
        }
        user.getDrunkenBeers().add(beer);
        userRepository.update(user);
    }

    @Override
    public Set<Beer> getWishList(int id) {
        User user = userRepository.getById(id);
        Set<Beer> wishList = userRepository.getWishList(user);
        return wishList;
    }

    @Override
    public void addBeerToUserWishList(int beerId, String username) {
        Beer beer = beerRepository.getById(beerId);
        User user = userRepository.getByUsername(username);
        if (user.getWishList().stream().anyMatch(b -> b.getId() == beerId)) {
            throw new DuplicateEntityException(String.format
                    ("Beer with name %s is already added to the wishList", beer.getName()));
        }
        user.getWishList().add(beer);
        userRepository.update(user);
    }

    @Override
    public void removeFromList(String list, int userId, int beerId) {
        userRepository.removeFromList(list, userId, beerId);
    }
}
