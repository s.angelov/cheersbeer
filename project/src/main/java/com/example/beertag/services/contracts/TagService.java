package com.example.beertag.services.contracts;

import com.example.beertag.models.Tag;
import com.example.beertag.repositories.contracts.TagRepository;

import java.util.List;

public interface TagService {

    List<Tag> getAll();

    Tag getById(int id);

    void create(Tag tag);

    void createWithoutError(Tag tag);

    void update(Tag tag);

    void delete(int id);

    void addTagToBeer(int tagId, int beerId);

    boolean tagExist(String tagName);

    boolean tagExist(int id);

    Tag getByTagName(String tag);
}
