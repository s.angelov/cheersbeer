package com.example.beertag.services.contracts;

public interface RatingService {
    void rate(String username, int beerId, int rating);

    void unrateBeer(String username, int beerId);

    double calculateRating(int beerId);

    String printAvgRating(int beerId);
}
