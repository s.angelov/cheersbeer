package com.example.beertag.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class CreateUserDto {

    @Size(min = 5, max = 30, message = "Name should be between 5 and 30 symbols")
    private String username;

    @Size(min = 4, max = 30, message = "Password should be between 4 and 30 symbols")
    private String password;

    @Size(min = 4, max = 30, message = "Password should be between 4 and 30 symbols")
    private String passwordConfirmation;



    public CreateUserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
