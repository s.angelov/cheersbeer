package com.example.beertag.models;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class BeerDto {

    private int id;

    @Size(min = 5,max =30 ,message = "Name should be between 5 and 30 symbols")
    private String name;

    @Size(min = 10,max = 255,message = "Description should be between 10 and 255 symbols")
    private String description;

    @PositiveOrZero(message="ABV must be a non negative number.")
    private double abv;

    @PositiveOrZero(message="StyleId must be positive.")
    private int styleId;

    @Size(min = 3,max =35 ,message = "Brewery Name should be between 3 and 35 symbols")
    private String brewery;

    private int originCountry;

    public BeerDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public int getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(int originCountry) {
        this.originCountry = originCountry;
    }
}
