package com.example.beertag.models;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class CreateBeerDto {
    public static final String DEFAULT_BEER_AVATAR = "/images/defaultBeerAvatar.jpg";

    @NotEmpty
    @Size(min = 5,max =30 ,message = "Name should be between 5 and 30 symbols")
    private String name;

    @PositiveOrZero(message="ABV must be a non negative number.")
    private double abv;

    @Size(min = 10,max = 2000,message = "Description should be between 10 and 2000 symbols")
    private String description;

    @Size(min = 3,max =35 ,message = "Brewery Name should be between 3 and 35 symbols")
    private String brewery;

    @Positive(message="Please choose a style")
    private int styleId;
    @Positive(message="Please choose origin country")
    private int countryId;

    private String hashtags;

    private String picture;

    public CreateBeerDto() {
        setPicture(DEFAULT_BEER_AVATAR);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getHashtags() {
        return hashtags;
    }

    public void setHashtags(String hashtags) {
        this.hashtags = hashtags;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }

    public String getPicture() {
        if (this.picture == null || this.picture.isEmpty() || this.picture.isBlank()) {
            return "/images/defaultBeerAvatar.jpg";
        }
        return picture;
    }

    public void setPicture(String picture) {
        if (this.picture == null || this.picture.isEmpty()) {
            this.picture ="/images/defaultBeerAvatar.jpg";
        } else{
            this.picture = picture;
        }
    }
}
