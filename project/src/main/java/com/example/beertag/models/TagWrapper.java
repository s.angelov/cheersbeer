package com.example.beertag.models;

import java.util.Set;

public class TagWrapper {

    Set<String> tags;

    public TagWrapper() {
    }

    public TagWrapper(Set<String> tags) {
        this.tags = tags;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }
}
