package com.example.beertag.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="beers")
public class Beer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="beer_id")
    private int id;

    @Column(name="beer_name")
    private String name;

    @Column(name="description")
    private String description;

    @ManyToOne
    @JoinColumn(name="origincountry_id")
    private OriginCountry originCountry;

    @ManyToOne
    @JoinColumn(name="brewery_id")
    private Brewery brewery;

    @ManyToOne
    @JoinColumn(name="style_id")
    private Style style;

    @Column(name="alcoholbyvolume")
    private double abv;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="creator_id")
    private User creator;

    //@JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JoinTable(
            name = "taggedbeers",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @Column(name ="images")
    private String picture;

    @JsonIgnore
    @Column(name = "deleted")
    private boolean deleted;


    //TODO
    @Formula(
            "(SELECT AVG(d.rating) FROM beers b " +
                    "JOIN drunkbeers d ON b.beer_id = d.beer_id " +
                    "WHERE d.beer_id = beer_id)"
    )
    private Double rating;


    public Beer() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OriginCountry getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(OriginCountry originCountry) {
        this.originCountry = originCountry;
    }

    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public double getRatingInPercentage() {
        double rating = this.rating;

        return rating / 5 * 100;
    }

    public String getPicture() {
        if (this.picture == null || this.picture.isEmpty()) {
            return "/images/defaultBeerAvatar.jpg";
        }
        return picture;
    }

    public void setPicture(String picture) {
        if (this.picture == null || this.picture.isEmpty()) {
            this.picture = "/images/defaultBeerAvatar.jpg";
        } else{
            this.picture = picture;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return id == beer.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String printTags() {
        StringBuilder sb = new StringBuilder();
        this.tags.stream().map(Tag::getTag).forEach(t ->{
            sb.append("#").append(t).append(" ");
        });
        return sb.toString();
    }

}
