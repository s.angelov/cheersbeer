package com.example.beertag.models;


import javax.persistence.*;

@Entity
@Table(name="styles")
public class Style {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="style_id")
    private int id;

    @Column(name="style")
    private String style;


    public Style() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }
}
