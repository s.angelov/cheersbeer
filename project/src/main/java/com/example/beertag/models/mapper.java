package com.example.beertag.models;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.InvalidOperationException;
import com.example.beertag.services.contracts.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class mapper {

    public static UserDto userToDto(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setUserName(user.getUserName());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());
        return dto;
    }

    public static Beer toBeer(BeerDto beerDto, Beer beer, StyleService styleService,
                              OriginCountryService originCountryService, BreweryService breweryService) {
        beer.setId(beerDto.getId());
        beer.setName(beerDto.getName());
        beer.setAbv(beerDto.getAbv());
        beer.setDescription(beerDto.getDescription());
        OriginCountry originCountry = originCountryService.getById(beerDto.getOriginCountry());
        beer.setOriginCountry(originCountry);
        if (breweryService.getByBreweryName(beerDto.getBrewery()) == null) {
            breweryService.create(new Brewery(beerDto.getBrewery()));
        }
        Brewery brewery = breweryService.getByBreweryName(beerDto.getBrewery());
        beer.setBrewery(brewery);
        Style style = styleService.getById(beerDto.getStyleId());
        beer.setStyle(style);

        return beer;
    }

    public static Beer toBeerFromCreateBeerDto(CreateBeerDto beerDto, Beer beer, User user, StyleService styleService, BeerService beerService,
                                               OriginCountryService originCountryService, BreweryService breweryService, TagService tagService) {
        beer.setName(beerDto.getName());
        beer.setAbv(beerDto.getAbv());
        beer.setDescription(beerDto.getDescription());
        beer.setCreator(user);
//        if (beerDto.getPicture() != null) {
//            beer.setPicture(beerDto.getPicture());
//        } else{
//            beer.setPicture("/images/defaultAvatar.jpg");
//        }

        OriginCountry originCountry = originCountryService.getById(beerDto.getCountryId());
        beer.setOriginCountry(originCountry);
        if (breweryService.getByBreweryName(beerDto.getBrewery()) == null) {
            breweryService.create(new Brewery(beerDto.getBrewery()));
        }
        Brewery brewery = breweryService.getByBreweryName(beerDto.getBrewery());
        beer.setBrewery(brewery);
        Style style = styleService.getById(beerDto.getStyleId());
        beer.setStyle(style);

        try {
            beerService.create(beer, user);

            String[] tagsToAdd = beerDto.getHashtags().split(" ");
            Set<Tag> tagResult = mapTagsFromString(tagService, tagsToAdd);
            beer.setTags(tagResult);
            beerService.update(beer, user);
        } catch (InvalidOperationException | DuplicateEntityException | EntityHasBeenDeletedException e) {
            System.out.println(e.getMessage());
            throw new DuplicateEntityException("Beer with such name already exist");
        }

        return beer;
    }



    public static  Beer  editBeerFromDto(CreateBeerDto beerDto, Beer beer, User user, StyleService styleService, BeerService beerService,
                                  OriginCountryService originCountryService, BreweryService breweryService, TagService tagService) {
        beer.setName(beerDto.getName());
        beer.setAbv(beerDto.getAbv());
        beer.setDescription(beerDto.getDescription());
        if (breweryService.getByBreweryName(beerDto.getBrewery()) == null) {
            breweryService.create(new Brewery(beerDto.getBrewery()));
        }
        Brewery brewery = breweryService.getByBreweryName(beerDto.getBrewery());
        beer.setBrewery(brewery);
        Style style = styleService.getById(beerDto.getStyleId());
        beer.setStyle(style);
        OriginCountry originCountry = originCountryService.getById(beerDto.getCountryId());
        beer.setOriginCountry(originCountry);
        String[] tagsToAdd = beerDto.getHashtags().split(" ");
        Set<Tag> tagResult = mapTagsFromString(tagService, tagsToAdd);
        beer.setTags(tagResult);
        return beer;

    }

    public static User userDtoToUser(CreateUserDto dto) {
        User user = new User();
        user.setUserName(dto.getUsername());
        user.setPassword(dto.getPassword());
        return user;
    }

    private static Set<Tag> mapTagsFromString(TagService tagService, String[] tagsToAdd) {
        Set<Tag> tagResult = new HashSet<>();
        for (String stringTag : tagsToAdd) {
            if (!tagService.tagExist(stringTag)) {
                Tag tag = new Tag(stringTag);
                tagService.create(tag);
                tagResult.add(tag);
            } else {
                tagResult.add(tagService.getByTagName(stringTag));
            }
        }
        return tagResult;
    }


}
