package com.example.beertag.models;


import javax.persistence.*;

@Entity
@Table(name="breweries")
public class Brewery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="brewery_id")
    private  int id;

    @Column(name="brewery_name")
    private String brewery;

    public Brewery() {
    }

    public Brewery(String brewery) {
        this.brewery = brewery;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrewery() {
        return brewery;
    }

    public void setBrewery(String brewery) {
        this.brewery = brewery;
    }



}
