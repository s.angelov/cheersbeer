package com.example.beertag.repositories;

import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.drunkbeers.DrunkBeers;
import com.example.beertag.repositories.contracts.BeerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public void update(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beerToDelete = session.get(Beer.class, id);
            if (beerToDelete.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format
                        ("Beer %s is already deleted", beerToDelete.getName()));
            }
            beerToDelete.setDeleted(true);
            session.beginTransaction();
            session.update(beerToDelete);
            session.getTransaction().commit();
        }

    }

    @Override
    public Beer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException(String.format("Beer with id %d not found!", id));
            }
            if (beer.isDeleted()) {
                throw new EntityHasBeenDeletedException(String.format
                        ("Access denied. Beer %s has been deleted", beer.getName()));
            }
            return beer;
        }
    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer", Beer.class);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> getAllDeletedBeers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer", Beer.class);
            return query.list().stream().filter(Beer::isDeleted).collect(Collectors.toList());

        }
    }


    @Override
    public List<Beer> filterByName(String name) {
        if (name.isEmpty()) {
            return getAll();
        }

        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name like concat('%',:name,'%') order by name desc", Beer.class);
            query.setParameter("name", name);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> filterByStyle(int styleId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where style.id= :styleId", Beer.class);
            query.setParameter("styleId", styleId);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> filterByCountry(int countryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where originCountry.id=:countryId", Beer.class);
            query.setParameter("countryId", countryId);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> filterByCountry(String countryName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where originCountry.country like concat('%',:countryName,'%')", Beer.class);
            query.setParameter("countryName", countryName);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> sortByName() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by name asc", Beer.class);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public List<Beer> sortByAbv() {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer order by abv asc", Beer.class);
            return query.list().stream().filter(b -> !b.isDeleted()).collect(Collectors.toList());
        }
    }

    @Override
    public Set<Beer> sortByRating() {
        try (Session session = sessionFactory.openSession()) {
            Query<DrunkBeers> query = session.createQuery(
                    "from DrunkBeers order by rating", DrunkBeers.class);
            // тук имаме всички DrunkBeers
            List<DrunkBeers> beerListWithRating = query.list();
            Set<Beer> setBeers = new LinkedHashSet<>();
            //тук ще мапваме дрнкбирите към беерс
            for (DrunkBeers drunkBeer : beerListWithRating) {
                int beerId = drunkBeer.getId().getBeerId();
                try (Session sessionSecond = sessionFactory.openSession()) {
                    Beer beer = sessionSecond.get(Beer.class, beerId);
                    setBeers.add(beer);
                }
            }

            return setBeers;
        }
    }

    @Override
    public List<Beer> searchByCriteria(String name, int styleId, int countryId, String tag) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Beer> criteriaQuery = criteriaBuilder.createQuery(Beer.class);
            Root<Beer> beer = criteriaQuery.from(Beer.class);
            Join<Object, Object> tags = beer.join("tags");

            List<Predicate> predicateList = new ArrayList<>();

            if (!tag.isEmpty()) {
                Predicate tagsPredicate = criteriaBuilder.like(tags.get("tag"), tag);
                predicateList.add(tagsPredicate);
            }


            if (styleId != 0) {
                Predicate stylePredicate = criteriaBuilder.equal(beer.get("style").get("id"), styleId);
                predicateList.add(stylePredicate);
            }

            if (countryId != 0) {
                Predicate countryPredicate = criteriaBuilder.equal(beer.get("originCountry").get("id"), countryId);
                predicateList.add(countryPredicate);
            }

            Predicate namePredicate = criteriaBuilder.like(beer.get("name"), "%" + name + "%");
            predicateList.add(namePredicate);

            Predicate deletePredicate = criteriaBuilder.equal(beer.get("deleted"), 0);
            predicateList.add(deletePredicate);

            criteriaQuery.distinct(true).select(beer).where(predicateList.toArray(Predicate[]::new));


            criteriaQuery.orderBy(criteriaBuilder.asc(beer.get("name")));
            //criteriaQuery.orderBy(criteriaBuilder.asc(beer.get("abv")));

            TypedQuery<Beer> query = session.createQuery(criteriaQuery);
            return query.getResultList();
        }
    }

}
