package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> getAll();

    Tag getById(int id);

    Tag getByTagName(String tagName);

    void create(Tag tag);

    void update(Tag tag);

    void delete(int id);

}
