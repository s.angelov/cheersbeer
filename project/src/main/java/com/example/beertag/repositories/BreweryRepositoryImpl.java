package com.example.beertag.repositories;

import com.example.beertag.models.Brewery;
import com.example.beertag.models.OriginCountry;
import com.example.beertag.repositories.contracts.BreweryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;


@Repository
public class BreweryRepositoryImpl implements BreweryRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public BreweryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery", Brewery.class);
            return query.list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new EntityNotFoundException(String.format("Brewery with id %d doesn't exist", id));
            }
            return brewery;
        }
    }

    @Override
    public Brewery getByBreweryName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where brewery like :name", Brewery.class);
            query.setParameter("name", name);
            if (query.list().isEmpty()) {
                return null;
            }
            return query.list().get(0);

        }
    }

    @Override
    public void create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.save(brewery);
        }
    }

    @Override
    public void update(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(brewery);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery breweryToDelete = session.get(Brewery.class, id);
            session.beginTransaction();
            session.delete(breweryToDelete);
            session.getTransaction().commit();
        }
    }
}
