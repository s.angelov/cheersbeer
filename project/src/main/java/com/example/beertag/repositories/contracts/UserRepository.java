package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface UserRepository {

    void add(User user);

    User getById(int id);

    List<User> filterByUsername(String name);

    List<User> getAll();

    List<User> getAllDeletedUsers();

    void update(User user);

    void delete(int id);

    Set<Beer> getDrunkenBeers(User user);

    Set<Beer> getDrunkenBeers(int userId);

    User getByUsername(String username);

    Set<Beer> getWishList (User user);

    Set<Beer> getWishList(int userId);

    void removeFromList(String list, int userId, int beerId);
}
