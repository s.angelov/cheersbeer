package com.example.beertag.repositories.contracts;


import java.util.List;

public interface CommonRepository<E> {

    List<E> getAll();

    E getById(int id);

    void create(E e);

    void update(E e);

    void delete(int id);
}
