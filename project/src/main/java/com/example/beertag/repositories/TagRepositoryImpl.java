package com.example.beertag.repositories;

import com.example.beertag.models.Tag;
import com.example.beertag.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag order by id asc", Tag.class);
            return query.list();
        }
    }

    @Override
    public Tag getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (tag == null) {
                throw new EntityNotFoundException(String.format("Tag with id %d doesn't exist", id));
            }
            return tag;
        }
    }

    @Override
    public Tag getByTagName(String tagName) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where tag like :tagName", Tag.class);
            query.setParameter("tagName", tagName);
            List<Tag> tags = query.list();
            if (tags.isEmpty()) {
                return null;
            } else {
                return tags.get(0);
            }
        }
    }

    @Override
    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.save(tag);
        }
    }

    @Override
    public void update(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tagToDelete = session.get(Tag.class, id);
            session.beginTransaction();
            session.delete(tagToDelete);
            session.getTransaction().commit();
        }
    }
}
