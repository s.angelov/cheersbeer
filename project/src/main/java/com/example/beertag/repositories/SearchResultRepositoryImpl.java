package com.example.beertag.repositories;

import com.example.beertag.models.Beer;
import com.example.beertag.repositories.contracts.SearchResultRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class SearchResultRepositoryImpl implements SearchResultRepository {
    private List<Beer> searchResult = new ArrayList<>();

    private String lastSortedBy = "name";

    public SearchResultRepositoryImpl() {
    }

    @Override
    public List<Beer> getSearchResult() {

        return searchResult;
    }

    @Override
    public void setSearchResult(List<Beer> searchResult) {
        this.searchResult = searchResult;
    }

    @Override
    public String getLastSortedBy() {
        return lastSortedBy;
    }

    @Override
    public void setLastSortedBy(String lastSortedBy) {
        this.lastSortedBy = lastSortedBy;
    }
}
