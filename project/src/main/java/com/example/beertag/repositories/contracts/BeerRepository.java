package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Beer;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface BeerRepository {

    void create(Beer beer);

    void update(Beer beer);

    void delete(int id);

    Beer getById(int id);

    List<Beer> getAll();

    List<Beer> getAllDeletedBeers();

    List<Beer> filterByName(String name);

    List<Beer> filterByStyle(int styleId);

    List<Beer> filterByCountry(int countryId);

    List<Beer> filterByCountry(String countryName);

    List<Beer> sortByName ();

    List<Beer> sortByAbv ();

    Set<Beer> sortByRating();

    List<Beer> searchByCriteria(String name, int styleId, int countryId, String tag);
}
