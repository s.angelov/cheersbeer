package com.example.beertag.controllers;

import com.example.beertag.models.CreateUserDto;
import com.example.beertag.models.User;
import com.example.beertag.models.mapper;
import com.example.beertag.services.contracts.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegisterController {
    private UserDetailsManager userDetailsManager;
    private final UserService userService;

    public RegisterController(UserDetailsManager userDetailsManager, UserService userService) {
        this.userDetailsManager = userDetailsManager;
        this.userService = userService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("createUserDto", new CreateUserDto());
        return ("register");
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute CreateUserDto userDto, BindingResult bindingResult, Model model) {
        String errorMessage;
        if (bindingResult.hasErrors()) {
            model.addAttribute("createUserDto", userDto);
            model.addAttribute("error", "username/password can't be empty!");
            return "register";
        }

        if (userDetailsManager.userExists(userDto.getUsername())) {
            model.addAttribute("createUserDto", userDto);
            errorMessage = "Username is taken";
            model.addAttribute("error", errorMessage = "Username is taken");
            return "register";
        }

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("createUserDto", userDto);
            errorMessage = "Make sure you have entered the same password twice";
            model.addAttribute("error", errorMessage = "Make sure you have entered the same password twice");
            return "register";
        }
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(userDto.getUsername(), "{noop}" + userDto.getPassword(), authorities);
        userDetailsManager.createUser(newUser);


        User user = mapper.userDtoToUser(userDto);
        userService.create(user);

        return "register_confirmation";
    }
}
