package com.example.beertag.controllers.rest;


import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.models.UserDto;
import com.example.beertag.models.mapper;
import com.example.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService service;

    @Autowired
    public UserRestController(UserService service) {
        this.service = service;
    }

    @GetMapping
    public List<UserDto> getAll() {
        return service.getAll().stream().map(mapper::userToDto).collect(Collectors.toList());
    }

    @GetMapping("/deleted")
    public List<UserDto> getAllDeletedUsers() {
        return service.getAllDeletedUsers().stream().map(mapper::userToDto).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public UserDto getById(@PathVariable int id) {
        try {
            User user = service.getById(id);
            return mapper.userToDto(user);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User create(@Valid @RequestBody User user) {
        try {
            service.create(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public User update(@Valid @RequestBody User user) {
        try {
            service.update(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{userId}/drunkenlist")
    public Set<Beer> getDrunkenList(@PathVariable int userId) {
        try {
            return service.getDrunkenBeers(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/addtodrunkenlist/{beerId}")
    public void addBeerToDrunkenList(@PathVariable int beerId, @RequestHeader("Authorization") String username) {
        try {
            service.addBeerToUserDrunkenList(beerId, username);
        } catch (EntityNotFoundException | DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @GetMapping("/{userId}/wishlist")
    public Set<Beer> getWishList(@PathVariable int userId) {
        try {
            return service.getWishList(userId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PutMapping("/addtowishlist/{beerId}")
    public void addBeerToWishList(@PathVariable int beerId, @RequestHeader("Authorization") String username) {
        try {
            service.addBeerToUserWishList(beerId, username);
        } catch (EntityNotFoundException | DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @DeleteMapping("/{beerId}/{userId}/removefromwishlist")
    public Set<Beer> removeFromWishList(@PathVariable int beerId, Model model, @PathVariable int userId) {

        try {
            service.removeFromList("wishlist", userId, beerId);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return service.getWishList(userId);
    }

}
