package com.example.beertag.controllers.rest;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Style;
import com.example.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/styles")
public class StyleController {

    private final StyleService service;

    @Autowired
    public StyleController(StyleService service) {
        this.service = service;
    }

    @GetMapping
    public List<Style> getAll() {
        return service.getAll();
    }

    @GetMapping("/{id}")
    public Style getById(@PathVariable int id) {
        return service.getById(id);
    }

    @PostMapping
    public Style create(@Valid @RequestBody Style style) {
        try {
            service.create(style);
            return style;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public Style update(@Valid @RequestBody Style style) {
        try {
            service.update(style);
            return style;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Style delete(@PathVariable int id) {
        try {
            Style styleToDelete = service.getById(id);
            service.delete(id);
            return styleToDelete;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}
