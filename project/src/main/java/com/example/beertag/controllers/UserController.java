package com.example.beertag.controllers;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityHasBeenDeletedException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.models.UserDto;
import com.example.beertag.services.contracts.BeerService;
import com.example.beertag.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/users")
public class UserController {
    private final UserService userService;
    private final BeerService beerService;

    public UserController(UserService userService, BeerService beerService) {
        this.userService = userService;
        this.beerService = beerService;
    }

    @GetMapping("/{userId}/wishlist")
    public String getWishList(@PathVariable String userId, Model model, Principal principal) {
        User loggedUser = mapFromPrincipalToUser(principal);
        model.addAttribute("user", loggedUser);
        int id;
        try {
            id = Integer.parseInt(userId);
        } catch (NumberFormatException e) {
            User user = userService.getByUsername(userId);
            id = user.getId();
        }

        try {
            String myList = "My wishlist";
            String buttonSign = "Add beers to wish list";

            Set<Beer> wishList = userService.getWishList(id);
            model.addAttribute("userWishList", wishList);
            model.addAttribute("myList", myList);
            model.addAttribute("buttonSign", buttonSign);
        } catch (EntityNotFoundException e) {
            model.addAttribute("exceptionMessage", e.getMessage());
            return "errorPage";
        }
        return "wishlist";
    }

    @GetMapping("/{userId}/drunklist")
    public String getDrunkList(@PathVariable String userId, Model model, Principal principal) {
        User loggedUser = mapFromPrincipalToUser(principal);
        model.addAttribute("user", loggedUser);
        int id;
        try {
            id = Integer.parseInt(userId);
        } catch (NumberFormatException e) {
            User user = userService.getByUsername(userId);
            id = user.getId();
        }

        try {
            String myList = "My drunklist";
            String buttonSign = "Add beers to drunk list";

            Set<Beer> wishList = userService.getDrunkenBeers(id);
            model.addAttribute("userWishList", wishList);
            model.addAttribute("myList", myList);
            model.addAttribute("buttonSign", buttonSign);

        } catch (EntityNotFoundException e) {
            model.addAttribute("exceptionMessage", e.getMessage());
            return "errorPage";
        }

        return "wishlist";
    }

    @GetMapping("/profile/{userId}")
    public String getProfilePage(@PathVariable String userId, Model model) {
        String errorMessage;
        User user;
        int id;
        try {
            id = Integer.parseInt(userId);
            user = userService.getById(id);
        } catch (NumberFormatException e) {
            user = userService.getByUsername(userId);
            id = user.getId();
        }
        try {
            int finalId = id;
            Set<Beer> createdBeers = beerService.getAll().stream().filter(b -> b.getCreator().getId() == finalId).collect(Collectors.toSet());
            Set<Beer> drunkList = userService.getDrunkenBeers(id);
            Set<Beer> wishList = userService.getWishList(id);
            model.addAttribute("user", user);
            model.addAttribute("wishlist", wishList);
            model.addAttribute("drunklist", drunkList);
            model.addAttribute("createdBeers", createdBeers);
        } catch (EntityNotFoundException | EntityHasBeenDeletedException e) {
            errorMessage = e.getMessage();
        }
        return "profile_page";
    }

    @GetMapping("/edit/{username}")
    public String getEditProfilePage(@PathVariable String username,
                                     Model model, Principal principal) {
        UserDto userDto = new UserDto();
        User user = mapFromPrincipalToUser(principal);
        userDto.setId(user.getId());
        userDto.setUserName(user.getUserName());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        model.addAttribute("user", user);
        model.addAttribute("userDto", userDto);
        return "edit_profile";
    }

    @PostMapping("/edit/{username}")
    public String editUserProfile(@PathVariable String username, @Valid @ModelAttribute UserDto userDto, BindingResult bindingResult,
                                  Model model, Principal principal) {
        User user = mapFromPrincipalToUser(principal);
        model.addAttribute("user", user);
        model.addAttribute("userDto", userDto);
        if (bindingResult.hasErrors()) {
            return "edit_profile";
        }
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        userService.update(user);
        return "redirect:/users/profile/" + principal.getName();
    }

    private User mapFromPrincipalToUser(Principal principal) {
        User user = null;
        try {
            user = userService.getByUsername(principal.getName());
        } catch (EntityNotFoundException | EntityHasBeenDeletedException | NullPointerException e) {

        }
        return user;
    }
}
