![alt text][logo]

[logo]: Logo/Header.jpg "TelerikAcademy"

# CheersBeer

Your task is to develop BEER TAG web application. BEER TAG enables your users to manage all
the beers that they have drank and want to drink. Each beer has detailed information about it from
the ABV (alcohol by volume) to the style and description. 